# PushNotification App
Simple web app for create push-notification.

![Main_page](https://test.stroygrad66.ru/main_page.jpg)



## Available commands:

- `npm start` - start dev server and open browser
- `npm build` - make a production build
